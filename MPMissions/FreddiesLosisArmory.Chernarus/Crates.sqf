

_crate = RIFLES;


clearMagazineCargo _crate;
clearWeaponCargo _crate;

_crate addWeaponCargo ["M16A2", 100];
_crate addWeaponCargo ["M16A2GL", 100];
_crate addWeaponCargo ["M16A4", 100];
_crate addWeaponCargo ["M16A4_ACG_GL", 100];
_crate addWeaponCargo ["M4A1_AIM_SD_camo", 100];
_crate addWeaponCargo ["M4A1_HWS_GL_SD_Camo", 100];
_crate addWeaponCargo ["M4A3_CCO_EP1", 100];
_crate addWeaponCargo ["G36K_camo", 100];
_crate addWeaponCargo ["G36C_camo", 100];
_crate addWeaponCargo ["G36A_camo", 100];
_crate addWeaponCargo ["G36_C_SD_camo", 100];
_crate addWeaponCargo ["AKS_74_UN_kobra", 100];
_crate addWeaponCargo ["AKS_74_kobra", 100];
_crate addWeaponCargo ["AKS_GOLD", 100];
_crate addWeaponCargo ["AK_107_GL_kobra", 100];
_crate addWeaponCargo ["AK_107_GL_pso", 100];
_crate addWeaponCargo ["M8_carbine", 100];
_crate addWeaponCargo ["M8_carbineGL", 100];
_crate addWeaponCargo ["AKS_74_GOSHAWK", 100];
_crate addWeaponCargo ["AKS_74_NSPU", 100];
_crate addWeaponCargo ["FN_FAL", 100];
_crate addWeaponCargo ["FN_FAL_ANPVS4", 100];
_crate addWeaponCargo ["LeeEnfield", 100];
_crate addWeaponCargo ["SCAR_H_CQC_CCO", 100];
_crate addWeaponCargo ["SCAR_H_CQC_CCO_SD", 100];
_crate addWeaponCargo ["SCAR_H_STD_EGLM_Spect", 100];
_crate addWeaponCargo ["SCAR_L_CQC", 100];
_crate addWeaponCargo ["SCAR_L_CQC_CCO_SD", 100];
_crate addWeaponCargo ["SCAR_L_CQC_EGLM_Holo", 100];
_crate addWeaponCargo ["SCAR_L_STD_EGLM_RCO", 100];
_crate addWeaponCargo ["SCAR_L_STD_EGLM_TWS", 100];
_crate addWeaponCargo ["SCAR_L_STD_Mk4CQT", 100];
_crate addWeaponCargo ["Sa58P_EP1", 100];
_crate addWeaponCargo ["Sa58V_CCO_EP1", 100];
_crate addWeaponCargo ["Sa58V_EP1", 100];
_crate addWeaponCargo ["Sa58V_RCO_EP1", 100];
_crate addWeaponCargo ["BAF_L85A2_RIS_ACOG", 100];
_crate addWeaponCargo ["BAF_L85A2_RIS_CWS", 100];
_crate addWeaponCargo ["BAF_L85A2_RIS_Holo", 100];
_crate addWeaponCargo ["m8_tws_sd", 100];
_crate addWeaponCargo ["m8_tws", 100];
_crate addWeaponCargo ["m8_holo_sd", 100];
_crate addWeaponCargo ["m8_carbine_pmc", 100];
_crate addWeaponCargo ["M14_EP1", 100];


_crate addMagazineCargo ["30Rnd_556x45_Stanag", 999];
_crate addMagazineCargo ["30Rnd_556x45_StanagSD", 999];
_crate addMagazineCargo ["30Rnd_556x45_G36", 999];
_crate addMagazineCargo ["30Rnd_556x45_G36SD", 999];
_crate addMagazineCargo ["30Rnd_545x39_AK", 999];
_crate addMagazineCargo ["30Rnd_545x39_AKSD", 999];
_crate addMagazineCargo ["FlareGreen_GP25", 999];
_crate addMagazineCargo ["FlareWhite_GP25", 999];
_crate addMagazineCargo ["FlareRed_GP25", 999];
_crate addMagazineCargo ["FlareGreen_M203", 999];
_crate addMagazineCargo ["FlareRed_M203", 999];
_crate addMagazineCargo ["FlareWhite_M203", 999];
_crate addMagazineCargo ["1Rnd_HE_GP25", 999];
_crate addMagazineCargo ["1Rnd_HE_M203", 999];
_crate addMagazineCargo ["30Rnd_762x39_SA58", 999];
_crate addMagazineCargo ["20Rnd_762x51_B_SCAR", 999];
_crate addMagazineCargo ["20Rnd_762x51_SB_SCAR", 999];
_crate addMagazineCargo ["20Rnd_762x51_FNFAL", 999];
_crate addMagazineCargo ["20Rnd_762x51_DMR", 999];
_crate addMagazineCargo ["10x_303", 999];

_crate2 = MG;

clearMagazineCargo _crate2;
clearWeaponCargo _crate2;


_crate2 addWeaponCargo ["M249_EP1", 100];
_crate2 addWeaponCargo ["M249_TWS_EP1", 100];
_crate2 addWeaponCargo ["M249_m145_EP1", 100];
_crate2 addWeaponCargo ["M60A4_EP1", 100];
_crate2 addWeaponCargo ["MG36_camo", 100];
_crate2 addWeaponCargo ["Mk_48_DES_EP1", 100];
_crate2 addWeaponCargo ["m240_scoped_EP1", 100];
_crate2 addWeaponCargo ["M240", 100];
_crate2 addWeaponCargo ["M8_SAW", 100];
_crate2 addWeaponCargo ["PK", 100];
_crate2 addWeaponCargo ["Pecheneg", 100];
_crate2 addWeaponCargo ["BAF_L110A1_Aim", 100];
_crate2 addWeaponCargo ["BAF_L7A2_GPMG", 100];
_crate2 addWeaponCargo ["BAF_L86A2_ACOG", 100];
_crate2 addMagazineCargo ["200Rnd_556x45_M249", 999];
_crate2 addMagazineCargo ["100Rnd_762x51_M240", 999];
_crate2 addMagazineCargo ["100Rnd_556x45_BetaCMag", 999];
_crate2 addMagazineCargo ["75Rnd_545x39_RPK", 999];
_crate2 addMagazineCargo ["100Rnd_762x54_PK", 999];
_crate2 addMagazineCargo ["200Rnd_556x45_L110A1", 999];
_crate2 addMagazineCargo ["30Rnd_556x45_Stanag", 999];

_crate9 = SHOTGUN;

clearMagazineCargo _crate9;
clearWeaponCargo _crate9;


_crate9 addWeaponCargo ["M1014", 100];
_crate9 addWeaponCargo ["Saiga12K", 100];
_crate9 addWeaponCargo ["AA12_PMC", 100];
_crate9 addMagazineCargo ["8Rnd_B_Beneli_74Slug", 999];
_crate9 addMagazineCargo ["8Rnd_B_Saiga12_74Slug", 999];
_crate9 addMagazineCargo ["8Rnd_B_Saiga12_Pellets", 999];
_crate9 addMagazineCargo ["8Rnd_B_Beneli_Pellets", 999];
_crate9 addMagazineCargo ["20Rnd_B_AA12_74Slug", 999];
_crate9 addMagazineCargo ["20Rnd_B_AA12_HE", 999];
_crate9 addMagazineCargo ["20Rnd_B_AA12_Pellets", 999];





_crate6 = SNIPER;

clearMagazineCargo _crate6;
clearWeaponCargo _crate6;

_crate6 addWeaponCargo ["Huntingrifle", 100];
_crate6 addWeaponCargo ["M110_NVG_EP1", 100];
_crate6 addWeaponCargo ["M110_TWS_EP1", 100];
_crate6 addWeaponCargo ["M249_m145_EP1", 100];
_crate6 addWeaponCargo ["M24_des_EP1", 100];
_crate6 addWeaponCargo ["SCAR_H_LNG_Sniper", 100];
_crate6 addWeaponCargo ["SCAR_H_LNG_Sniper_SD", 100];
_crate6 addWeaponCargo ["SCAR_H_STD_TWS_SD", 100];
_crate6 addWeaponCargo ["SVD_NSPU_EP1", 100];
_crate6 addWeaponCargo ["SVD_des_EP1", 100];
_crate6 addWeaponCargo ["SVD", 100];
_crate6 addWeaponCargo ["m107_TWS_EP1", 100];
_crate6 addWeaponCargo ["DMR", 100];
_crate6 addWeaponCargo ["M107", 100];
_crate6 addWeaponCargo ["KSVK", 100];
_crate6 addWeaponCargo ["M40A3", 100]; 
_crate6 addWeaponCargo ["M4SPR", 100];
_crate6 addWeaponCargo ["M8_sharpshooter", 100]; 
_crate6 addWeaponCargo ["VSS_Vintorez", 100];
_crate6 addWeaponCargo ["BAF_AS50_TWS", 100]; 
_crate6 addWeaponCargo ["BAF_AS50_scoped", 100];
_crate6 addWeaponCargo ["BAF_LRR_scoped", 100];
_crate6 addWeaponCargo ["BAF_LRR_scoped_W", 100]; 
_crate6 addMagazineCargo ["20Rnd_762x51_B_SCAR", 999];
_crate6 addMagazineCargo ["5Rnd_762x51_M24", 999];
_crate6 addMagazineCargo ["10Rnd_762x54_SVD", 999];
_crate6 addMagazineCargo ["10Rnd_127x99_m107", 999];
_crate6 addMagazineCargo ["5Rnd_127x108_KSVK", 999];
_crate6 addMagazineCargo ["20Rnd_762x51_DMR", 999]; 
_crate6 addMagazineCargo ["20Rnd_556x45_Stanag", 999]; 
_crate6 addMagazineCargo ["30Rnd_556x45_G36", 999]; 
_crate6 addMagazineCargo ["20Rnd_9x39_SP5_VSS", 999];
_crate6 addMagazineCargo ["20Rnd_762x51_B_SCAR", 999];
_crate6 addMagazineCargo ["5Rnd_127x99_as50", 999];
_crate6 addMagazineCargo ["5Rnd_86x70_L115A1", 999]; 
_crate6 addMagazineCargo ["20Rnd_762x51_SB_SCAR", 999];
_crate6 addMagazineCargo ["5x_22_LR_17_HMR", 999];

_crateSUB = SUB;

clearMagazineCargo _crateSUB;
clearWeaponCargo _crateSUB;

_crateSUB addWeaponCargo ["Bizon", 100];
_crateSUB addWeaponCargo ["bizon_silenced", 100];
_crateSUB addWeaponCargo ["MP5SD", 100];
_crateSUB addWeaponCargo ["MP5A5", 100];
_crateSUB addWeaponCargo ["M8_compact", 100];

_crateSUB addMagazineCargo ["64Rnd_9x19_SD_Bizon", 999];
_crateSUB addMagazineCargo ["30Rnd_9x19_MP5", 999];
_crateSUB addMagazineCargo ["30Rnd_9x19_MP5SD", 999];
_crateSUB addMagazineCargo ["64Rnd_9x19_Bizon", 999];
_crateSUB addMagazineCargo ["30Rnd_556x45_G36", 999];


_crate3 = LAUNCHER;

clearMagazineCargo _crate3;
clearWeaponCargo _crate3;


_crate3 addWeaponCargo ["Javelin", 100];
_crate3 addWeaponCargo ["Stinger", 100];
_crate3 addWeaponCargo ["Igla", 100];
_crate3 addWeaponCargo ["M136", 100];
_crate3 addWeaponCargo ["M47Launcher_EP1", 100];
_crate3 addWeaponCargo ["MAAWS", 100];
_crate3 addWeaponCargo ["RPG7V", 100];
_crate3 addWeaponCargo ["MetisLauncher", 100];
_crate3 addWeaponCargo ["RPG18", 100];
_crate3 addWeaponCargo ["Strela", 100];
_crate3 addWeaponCargo ["BAF_NLAW_Launcher", 100];
_crate3 addWeaponCargo ["Mk13_EP1", 100];
_crate3 addWeaponCargo ["M32_EP1", 100];
_crate3 addWeaponCargo ["M79_EP1", 100];

_crate3 addMagazineCargo ["Javelin", 999];
_crate3 addMagazineCargo ["Stinger", 999];
_crate3 addMagazineCargo ["Igla", 999];
_crate3 addMagazineCargo ["M136", 999];
_crate3 addMagazineCargo ["Dragon_EP1", 999];
_crate3 addMagazineCargo ["20Rnd_762x51_DMR", 999]; 
_crate3 addMagazineCargo ["MAAWS_HEAT", 999]; 
_crate3 addMagazineCargo ["MAAWS_HEDP", 999]; 
_crate3 addMagazineCargo ["AT13", 999];
_crate3 addMagazineCargo ["PG7V", 999];
_crate3 addMagazineCargo ["RPG18", 999];
_crate3 addMagazineCargo ["STRELA", 999];
_crate3 addMagazineCargo ["STRELA", 999];
_crate3 addMagazineCargo ["NLAW", 999];
_crate3 addMagazineCargo ["1Rnd_HE_M203", 999];
_crate3 addMagazineCargo ["6Rnd_HE_M203", 999]; 
_crate3 addMagazineCargo ["1Rnd_HE_M203", 999]; 



_crate5 = Pistols;

clearMagazineCargo _crate5;
clearWeaponCargo _crate5;


_crate5 addWeaponCargo ["glock17_EP1", 100];
_crate5 addWeaponCargo ["Colt1911", 100];
_crate5 addWeaponCargo ["M9", 100];
_crate5 addWeaponCargo ["M9SD", 100];
_crate5 addWeaponCargo ["Makarov", 100];
_crate5 addWeaponCargo ["MakarovSD", 100];
_crate5 addWeaponCargo ["Revolver_ep1", 100];
_crate5 addWeaponCargo ["revolver_gold_ep1", 100];
_crate5 addWeaponCargo ["Sa61_EP1", 100];
_crate5 addWeaponCargo ["UZI_EP1", 100];
_crate5 addWeaponCargo ["UZI_SD_EP1", 100];
_crate5 addMagazineCargo ["17Rnd_9x19_glock17", 999];
_crate5 addMagazineCargo ["7Rnd_45ACP_1911", 999];
_crate5 addMagazineCargo ["6Rnd_45ACP", 999];
_crate5 addMagazineCargo ["8Rnd_9x18_MakarovSD", 999];
_crate5 addMagazineCargo ["8Rnd_9x18_Makarov", 999];
_crate5 addMagazineCargo ["15Rnd_9x19_M9SD", 999]; 
_crate5 addMagazineCargo ["15Rnd_9x19_M9", 999]; 
_crate5 addMagazineCargo ["30Rnd_9x19_UZI", 999]; 
_crate5 addMagazineCargo ["30Rnd_9x19_UZI_SD", 999];
_crate5 addMagazineCargo ["20Rnd_B_765x17_Ball", 999];




_crate4 = Misc;


clearMagazineCargo _crate4;
clearWeaponCargo _crate4;

_crate4 addWeaponCargo ["Binocular", 999];
_crate4 addWeaponCargo ["NVGoggles", 999];
_crate4 addWeaponCargo ["Laserdesignator", 999]; 
_crate4 addWeaponCargo ["Binocular_Vector", 999]; 
_crate4 addWeaponCargo ["ItemCompass", 999]; 
_crate4 addWeaponCargo ["ItemGPS", 999]; 
_crate4 addWeaponCargo ["ItemMAP", 999];
_crate4 addWeaponCargo ["ItemRadio", 999]; 
_crate4 addWeaponCargo ["ItemWatch", 999]; 
_crate4 addMagazineCargo ["HandGrenade", 999]; 
_crate4 addMagazineCargo ["MineE", 999]; 
_crate4 addMagazineCargo ["PipeBomb", 999]; 
_crate4 addMagazineCargo ["TimeBomb", 999]; 

