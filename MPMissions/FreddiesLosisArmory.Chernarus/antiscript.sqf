while {true} do
{
	{_x hideObject false} forEach AllUnits;
	player removeAllEventHandlers "HandleDamage";
	player removeAllEventHandlers "fired";
	player removeAllEventHandlers "hit";
	player removeAllEventHandlers "dammaged";
	if(typeOf Vehicle player!="S1203_TK_CIV_EP1") then
	{
		(vehicle player) removeAllEventHandlers "HandleDamage;
	};
	(vehicle player) removeAllEventHandlers "fired";
	(vehicle player) removeAllEventHandlers "hit";
	(vehicle player) removeAllEventHandlers "dammaged";
	disableuserinput false;
	sleep 1.0;
};