// start_cam.sqf
//  � MARCH 2011 - norrin

player addAction ["Switch on cam", "NORRN_spectate\init_specCam.sqf", "", 0, false, true];
_inVcl = 1234567891011;
_vcl = objNull;

while {true} do
{
	while {alive player} do 
	{
		if (isNull _vcl && vehicle player != player) then 
		{	
			_vcl = vehicle player;
			_inVcl = _vcl addAction ["Switch on cam", "NORRN_spectate\init_specCam.sqf", "", 0, false, true];	
		};
		if (!isNull _vcl && vehicle player == player) then 
		{
			_vcl removeAction _inVcl;
			_vcl = objNull;
		};
		sleep 1
	};
	if (!isNull _vcl) then 
	{
		_vcl removeAction _inVcl;
		_vcl = objNull;
	};
	sleep 1;
	waitUntil {alive player};
	[]execVM "NORRN_spectate\start_cam.sqf";
	if (true) exitWith {};
};