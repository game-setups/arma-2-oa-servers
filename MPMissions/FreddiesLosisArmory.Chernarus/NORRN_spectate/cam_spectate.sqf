/*
 CAM SPECTATE SCRIPT 

Gives the player an action to that allows the use of a spectator cam

 � MARCH 2011 - norrin
 
**************************************************************************************************************************
 Start cam_spectate.sqf
*/

disableserialization;

private ["_c","_friends","_sidePlayer","_pos","_camx","_camy","_camz","_angh","_angv","_dialog_0","_display","_index","_cam_angle","_cam_focus","_unit","_target","_free_pos"];
//define variables
NORRN_Spec_CAM_END			= false;
NORRN_CAM_NVG 				= false;
NORRN_FOCUS_CAM_ON 			= 0;
NORRN_Spec_CAM_TYPE 		= 0;
_c 							= 0; 
_friends 					= [];
_sidePlayer					= side player;

titletext ["Spectate Script Initialising","BLACK FADED",3];
showcinemaborder false;

//set camera to target body
_pos = [(getpos player select 0), (getpos player select 1), (getpos player select 2)+ 1];
_camx = _pos select 0;
_camy = _pos select 1;
_camz = _pos select 2;

NORRN_Spec_cam = "camera" CamCreate [_camx,_camy+5,_camz+1];
NORRN_Spec_cam CamSetTarget player;
NORRN_Spec_cam CameraEffect ["INTERNAL","Back"];
NORRN_Spec_cam CamCommit 2;

hint "";

//setVars for free camera 
_angh = 0; 
_angv = 45;
sleep 0.1;

{if (alive _x  && (side _x	== _sidePlayer)) then {_friends = _friends + [_x]}; sleep 0.005} forEach allUnits;
COUNT_CAM_friends = count _friends;

//create dialog and initialise keyboard;
_dialog_0 = createDialog "spectate_cam_dialog"; 
_display = findDisplay 99129;
_display displaySetEventHandler ["Keydown", "_this call CAM_KEY_pressed"];
titletext [" ","BLACK IN",1];
CAM_KEY_pressed = compile preprocessfile "NORRN_spectate\CAM_KEY_pressed.sqf";

//change camera via dialog
lbClear 10004;
_index = lbAdd[10004, "3rd Person"];
_index = lbAdd[10004, "Top Down"];
_index = lbAdd[10004, "Front Side"];
_index = lbAdd[10004, "1st Person"];
_index = lbAdd[10004, "Follow/Free"];
lbSetCurSel [10004, 0];

while {!NORRN_Spec_CAM_END} do 
{
	scopeName "FollowCam_01";
	
	//fix for changing dialogs
	_cam_angle = lbCurSel 10004;
	_cam_focus = lbCurSel 10005;
	
	if (_c == 25) then 
	{	
		_friends = [];
		{if (alive _x  && (side _x	== _sidePlayer)) then {_friends = _friends + [_x]}; sleep 0.005} forEach allUnits;
		_c = -1;
	};
	_c = _c + 1;
	COUNT_CAM_friends = count _friends;

	//change target via dialog
	lbClear 10005;
	{if (alive _x) then {_index_friends = lbAdd[10005, name _x]}} forEach _friends;
	lbSetCurSel [10005, lbCurSel 10005];
	
	NORRN_Spec_CAM_TYPE = lbCurSel 10004;
	NORRN_FOCUS_CAM_ON = lbCurSel 10005;
	
	if (!dialog) exitWith {};
	
	_unit = _friends select NORRN_FOCUS_CAM_ON; 
	_target = vehicle _unit;
	
	if (NORRN_Spec_CAM_TYPE == 0) then {_target switchCamera "EXTERNAL"; NORRN_Spec_cam CameraEffect ["Terminate","Back"];};
	if (NORRN_Spec_CAM_TYPE == 1) then {NORRN_Spec_cam camsettarget _target; NORRN_Spec_cam cameraeffect ["internal", "back"];NORRN_Spec_cam camsetrelpos [0, -2, OFPEC_range_to_unit];NORRN_Spec_cam camcommit 0.005};
	if (NORRN_Spec_CAM_TYPE == 2) then {NORRN_Spec_cam camsettarget _target; NORRN_Spec_cam cameraeffect ["internal", "back"];NORRN_Spec_cam camsetrelpos [-1.5, 3, 0.2];NORRN_Spec_cam camSetFov 1.1; NORRN_Spec_cam camcommit 0.005};
	if (NORRN_Spec_CAM_TYPE == 3) then {_target switchCamera "INTERNAL";NORRN_Spec_cam CameraEffect ["Terminate","Back"]};
	
	//========================================================================================
	// Free cam courtesy of hoz and mandoble see: OFPEC - see: http://www.ofpec.com/forum/index.php?topic=32970.0
	if (NORRN_Spec_CAM_TYPE == 4) then
	{ 	 
		NORRN_Spec_cam camsettarget _target; 
		NORRN_Spec_cam cameraeffect ["internal", "back"];

		if (OFPEC_MouseButtons select 1) then 
		{
			if (((OFPEC_MouseCoord select 0) >= 0) && ((OFPEC_MouseCoord select 0) <= 1) &&
			((OFPEC_MouseCoord select 1) >= 0) && ((OFPEC_MouseCoord select 1) <= 1)) then
	   		{
				_deltah = (0.5 - (OFPEC_MouseCoord select 0))*10/0.2;
				_deltav = (0.5 - (OFPEC_MouseCoord select 1))*10/0.2;
				_angv = (_angv + _deltav);
				_angh = (_angh + _deltah);
				_angv = _angv max 0;			
				_angv = _angv min 89;
			};						
		}; 
		_free_pos = [(getpos _target select 0) + sin(_angh)*OFPEC_range_to_unit, (getpos _target select 1) + cos(_angh)*OFPEC_range_to_unit, (getpos _target select 2) + OFPEC_range_to_unit*sin(_angv)];
		NORRN_Spec_cam camSetPos _free_pos;
		NORRN_Spec_cam camCommit 0.5;  
	};     
	//========================================================================================	
	sleep 0.02;
};

//reset view & destroy camera
camUseNVG false;
closeDialog 0;
vehicle player switchCamera "INTERNAL"; 
NORRN_Spec_cam CameraEffect ["Terminate","Back"];
CamDestroy NORRN_Spec_cam;
