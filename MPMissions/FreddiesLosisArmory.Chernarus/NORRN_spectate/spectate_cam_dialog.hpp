/*

Spectate CAMERA DIALOG

� MARCH 2011 - norrin  
**********************************************************************************************************************************
spectate_cam_dialog.hpp
*/

// Control types
#define CT_STATIC           0
#define CT_BUTTON           1
#define CT_EDIT             2
#define CT_SLIDER           3
#define CT_COMBO            4
#define CT_LISTBOX          5
#define CT_TOOLBOX          6
#define CT_CHECKBOXES       7
#define CT_PROGRESS         8
#define CT_HTML             9
#define CT_STATIC_SKEW      10
#define CT_ACTIVETEXT       11
#define CT_TREE             12
#define CT_STRUCTURED_TEXT  13
#define CT_CONTEXT_MENU     14
#define CT_CONTROLS_GROUP   15
#define CT_XKEYDESC         40
#define CT_XBUTTON          41
#define CT_XLISTBOX         42
#define CT_XSLIDER          43
#define CT_XCOMBO           44
#define CT_ANIMATED_TEXTURE 45
#define CT_OBJECT           80
#define CT_OBJECT_ZOOM      81
#define CT_OBJECT_CONTAINER 82
#define CT_OBJECT_CONT_ANIM 83
#define CT_LINEBREAK        98
#define CT_USER             99
#define CT_MAP              100
#define CT_MAP_MAIN         101


// Static styles
#define ST_POS            0x0F
#define ST_HPOS           0x03
#define ST_VPOS           0x0C
#define ST_LEFT           0x00
#define ST_RIGHT          0x01
#define ST_CENTER         0x02
#define ST_DOWN           0x04
#define ST_UP             0x08
#define ST_VCENTER        0x0c

#define ST_TYPE           0xF0
#define ST_SINGLE         0
#define ST_MULTI          16
#define ST_TITLE_BAR      32
#define ST_PICTURE        48
#define ST_FRAME          64
#define ST_BACKGROUND     80
#define ST_GROUP_BOX      96
#define ST_GROUP_BOX2     112
#define ST_HUD_BACKGROUND 128
#define ST_TILE_PICTURE   144
#define ST_WITH_RECT      160
#define ST_LINE           176

class spectate_cam_dialog 
{
	idd = 99129;   
	movingEnable = true;   
	
	class controls {
		class NORRNRscText {
			access = ReadAndWrite;
			type = 0;
			idc = -1;
			style = 0;
			x = 0; y = 0;
			w = 0; h = 0;
			font = "TahomaB";
			sizeEx = 0.02;
			colorBackground[] = {0, 0, 0, 0};
			colorText[] = {1, 1, 1, 1};
			text = "";
		};
	
		class NORRNRscActiveText {
			access = ReadAndWrite;
			type = 11;
			idc = -1;
			style = 2;
			x = 0; y = 0;
			h = 0; w = 0;
			font = "TahomaB";
			sizeEx = 0.04;
			color[] = {1, 1, 1, 1};
			colorActive[] = {1, 0.5, 0, 1};
			soundEnter[] = {"", 0.1, 1};
			soundPush[] = {"", 0.1, 1};
			soundClick[] = {"", 0.1, 1};
			soundEscape[] = {"", 0.1, 1};
			text = "";
			default = 0;
		};
	
		class NORRNRscControlsGroup {
			type = 15;
			idc = -1;
			style = 0;
			x = 0; y = 0;
			w = 0; h = 0;
	
			class VScrollbar {
				color[] = {1, 1, 1, 1};
				width = 0.021;
				autoScrollSpeed = -1;
				autoScrollDelay = 5;
				autoScrollRewind = false;	
			};
	
			class HScrollbar {
				color[] = {1, 1, 1, 1};
				height = 0.028;
				autoScrollSpeed = -1;
				autoScrollDelay = 5;
				autoScrollRewind = false;	
			};
	
			class ScrollBar {
			color[] = {1, 1, 1, 0.6};
			colorActive[] = {1, 1, 1, 1};
			colorDisabled[] = {1, 1, 1, 0.3};
			thumb = "\ca\ui\data\ui_scrollbar_thumb_ca.paa";
			arrowFull = "\ca\ui\data\ui_arrow_top_active_ca.paa";
			arrowEmpty = "\ca\ui\data\ui_arrow_top_ca.paa";
			border = "\ca\ui\data\ui_border_scroll_ca.paa";
			};
	
			class Controls {};
		};
	
		class NORRNRscCombo {	
			idc = -1;
			type = CT_COMBO;
			style = ST_LEFT; 
			colorSelect[] = {1, 1, 1, 1 };
			colorSelectBackground[] = {0, 0, 0, 1};
			colorText[] = {1, 1, 1, 1};
			colorScrollbar[] = {0, 0, 0, 1};
			colorBackground[] = {0, 0, 0, 1};
			colorBorder[] = {0, 0, 0, 1};
			colorShadow[] = {0, 0, 0, 1};
			soundSelect[] = { "", 0, 1 };
			soundExpand[] = { "", 0, 1 };
			soundCollapse[] = { "", 0, 1 };
			borderSize = 0;
			font = "TahomaB";
			sizeEx = 0.02; 
			rowHeight = 0.025;
			wholeHeight = 0.3;
			text = "";
			maxHistoryDelay = 0;
			default = true;
	
			x = 0; y = 0;
			w = 0; h = 0;
	
			thumb = "\ca\ui\data\ui_scrollbar_thumb_ca.paa";
			arrowFull = "\ca\ui\data\ui_arrow_top_active_ca.paa";
			arrowEmpty = "\ca\ui\data\ui_arrow_top_ca.paa";
			border = "\ca\ui\data\ui_border_scroll_ca.paa";
	
			autoScrollSpeed = -1;
			autoScrollDelay = 5;
			autoScrollRewind = 0;

			class ScrollBar {
				color[] = {1, 1, 1, 0.6};
				colorActive[] = {1, 1, 1, 1};
				colorDisabled[] = {1, 1, 1, 0.3};
				thumb = "\ca\ui\data\ui_scrollbar_thumb_ca.paa";
				arrowFull = "\ca\ui\data\ui_arrow_top_active_ca.paa";
				arrowEmpty = "\ca\ui\data\ui_arrow_top_ca.paa";
				border = "\ca\ui\data\ui_border_scroll_ca.paa";
			};
		};
	
		class mouseHandler : NORRNRscControlsGroup {			
			onMouseMoving = "[""MouseMoving"",_this] call MouseEvents";
			onMouseButtonDown = "[""MouseButtonDown"",_this] call MouseEvents";
			onMouseButtonUp = "[""MouseButtonUp"",_this] call MouseEvents";
			onMouseZChanged = "[""MouseZChanged"",_this] call MouseEvents";			
			idc = 2501;
			type = 15;
			style = 0;
			x = 0.0; y = 0.0;
			w = 1.0; h = 1.0;			
			colorBackground[] = {0.2, 0.0, 0.0, 0.0};
		
			class VScrollbar {
				color[] = {1, 1, 1, 1};
				width = 0.021;
				autoScrollSpeed = -1;
				autoScrollDelay = 5;
				autoScrollRewind = false;	
		
			};
			class HScrollbar {
				color[] = {1, 1, 1, 1};
				height = 0.028;
				autoScrollSpeed = -1;
				autoScrollDelay = 5;
				autoScrollRewind = false;	
			};
			class ScrollBar {
				color[] = {1, 1, 1, 0.6};
				colorActive[] = {1, 1, 1, 1};
				colorDisabled[] = {1, 1, 1, 0.3};
				thumb = "\ca\ui\data\ui_scrollbar_thumb_ca.paa";
				arrowFull = "\ca\ui\data\ui_arrow_top_active_ca.paa";
				arrowEmpty = "\ca\ui\data\ui_arrow_top_ca.paa";
				border = "\ca\ui\data\ui_border_scroll_ca.paa";
			};
	
			class Controls {};
		};

		class TOP_BORDER {
			idc = -1;
			type = CT_STATIC;
			style = ST_CENTER;
			x = -1.5; y = safeZoneY;
			w = 4.0; h = 0.15;
			font = "TahomaB";
			sizeEx = 0.04;
			colorText[] = { 1, 1, 1, 1 };
			colorBackground[] = {0,0,0,1};
			text = "";
		};

		class BOTTOM_BORDER	{
			idc = -1;
			type = CT_STATIC;
			style = ST_CENTER;
			x = -1.5; y = (safeZoneY + safeZoneH) -0.16;
			w = 4.0; h = 0.26;
			font = "TahomaB";
			sizeEx = 0.04;
			colorText[] = { 1, 1, 1, 1 };
			colorBackground[] = {0,0,0,1};
			text = "";
		};	

		class TITLE_DIALOG {
			idc = -1;
			type = CT_ACTIVETEXT;
			style = ST_LEFT;
			x = (safeZoneX + safeZoneW/2)-0.1; y = 0.03 + safeZoneY;
			w = 0.4; h = 0.04;
			font = "TahomaB";
			sizeEx = 0.04;
			color[] = { 1, 1, 1, 1 };
			colorActive[] = { 1, 0.2, 0.2, 1 };
			colorText[] = { 1, 1, 1, 1 };
			colorBackground[] = {0,0,0,1};
			soundEnter[] = { "", 0, 1 };   // no sound
			soundPush[] = { "", 0, 1 };
			soundClick[] = { "", 0, 1 };
			soundEscape[] = { "", 0, 1 };
			action = "";
			text = "Spectate Camera";
			default = true;
		};
	
		class PRESS_HELP : NORRNRscText {    
			idc = 10000;
			style = ST_MULTI; 
			linespacing = 1;
			x = (safeZoneX + safeZoneW) -0.3; y = (safeZoneY + safeZoneH) -0.11;
			w = 0.25; h = 0.1; 
			text = ""; 
		};
	
		class HELP_DIALOG : NORRNRscActiveText {
			idc = -1;
			style = ST_LEFT; 
			linespacing = 1;	
			x = (safeZoneX + safeZoneW) -0.3; y = (safeZoneY + safeZoneH) -0.14;
			w = 0.4; h = 0.02;
			sizeEx = 0.02;
			action = "ctrlSetText [10000, ""Keyboard controls:               A/D - Previous/Next target       W/S - Previous/Next camera        N - Toggle NV for Free Cam""]";
			text = "Press for Help";
		};
	
		class CAM_LIST: NORRNRscCombo  {
			idc = 10004;
			x = safeZoneX +0.09; y = safeZoneY +0.03;
			w = 0.15; h = 0.04;
			//sizeEx = 0.02; 
		};
	
		class CAM_select {
			idc = 10001;
			type = CT_STATIC;        
			style = ST_LEFT; 
			colorText[] = {1, 1, 1, 1};
			colorBackground[] = {0,0,0,0};
			x = safeZoneX +0.02; y = safeZoneY +0.03;
			w = 0.08; h = 0.04;
			font = "TahomaB";
			sizeEx = 0.02;
			text = "Camera:";
			default = true;
		};

		class FRIEND_LIST: NORRNRscCombo  {
			idc = 10005;
			x = (safeZoneX + safeZoneW)-0.18; y = safeZoneY +0.03;
			w = 0.15; h = 0.04;
			//sizeEx = 0.02; 
		};

		class FRIEND_select {	
			idc = 10002;
			type = CT_STATIC;        
			style = ST_LEFT; 
			colorText[] = {1, 1, 1, 1};
			colorBackground[] = {0,0,0,0};
			x = (safeZoneX + safeZoneW)-0.25; y = safeZoneY +0.03;
			w = 0.08; h = 0.04;
			font = "TahomaB";
			sizeEx = 0.02;
			text = "Target:";
			default = true;
		};
	};

};

