class TP_DIALOG
{
	idd = 1;
	movingenable = true;

	class controls
	{
		class RscText_1000: RscText
		{
			idc = 1000;
			x = 0.356771 * safezoneW + safezoneX;
			y = 0.307575 * safezoneH + safezoneY;
			w = 0.29362 * safezoneW;
			h = 0.412339 * safezoneH;
			colorBackground[] = {0.25,0.25,0.25,0.8};
		};
		class RscFrame_1800: RscFrame
		{
			idc = 1800;
			x = 0.363932 * safezoneW + safezoneX;
			y = 0.32132 * safezoneH + safezoneY;
			w = 0.279297 * safezoneW;
			h = 0.38485 * safezoneH;
		};
		class RscPicture_1200: RscPicture
		{
			idc = 1200;
			text = "map5.jpg";
			x = 0.371094 * safezoneW + safezoneX;
			y = 0.335064 * safezoneH + safezoneY;
			w = 0.264974 * safezoneW;
			h = 0.357361 * safezoneH;
		};
		class RscButton_1603: RscButton
		{
			idc = 1603;
			text = "Weapons";
			x = 0.4 * safezoneW + safezoneX;
			y = 0.5 * safezoneH + safezoneY;
			w = 0.0869048 * safezoneW;
			h = 0.0245142 * safezoneH;
			action="closeDialog 0;player setPos [4718.31,10237.2,0.00143433]";
		};
		class RscButton_1601: RscButton
		{
			idc = 1601;
			text = "Helicopters";
			x = 0.4 * safezoneW + safezoneX;
			y = 0.53 * safezoneH + safezoneY;
			w = 0.0869048 * safezoneW;
			h = 0.0245142 * safezoneH;
			action="closeDialog 0;player setPos [3733.69,10287,0]";
		};
		class RscButton_1602: RscButton
		{
			idc = 1602;
			text = "Tracked Vehicles";
			x = 0.4 * safezoneW + safezoneX;
			y = 0.56 * safezoneH + safezoneY;
			w = 0.0869048 * safezoneW;
			h = 0.0245142 * safezoneH;
			action="closeDialog 0;player setPos [4893.94,10050.1,0.00143433]";
		};
		class RscButton_1604: RscButton
		{
			idc = 1604;
			text = "Drone Launcher";
			x = 0.5 * safezoneW + safezoneX;
			y = 0.5 * safezoneH + safezoneY;
			w = 0.0869048 * safezoneW;
			h = 0.0245142 * safezoneH;
			action="closeDialog 0;player setPos [4766.77,10245.2,0.00143433]";
		};
		class RscButton_1605: RscButton
		{
			idc = 1605;
			text = "Jets";
			x = 0.5 * safezoneW + safezoneX;
			y = 0.53 * safezoneH + safezoneY;
			w = 0.0869048 * safezoneW;
			h = 0.0245142 * safezoneH;
			action="closeDialog 0;player setPos [4095,10830,0]";
		};
		class RscButton_1600: RscButton
		{
			idc = 1600;
			text = "Wheeled Vehicles";
			x = 0.5 * safezoneW + safezoneX;
			y = 0.56 * safezoneH + safezoneY;
			w = 0.0869048 * safezoneW;
			h = 0.0245142 * safezoneH;
			action="closeDialog 0;player setPos [4941.37,9967.62,0.00143433]";
		};
	};
};