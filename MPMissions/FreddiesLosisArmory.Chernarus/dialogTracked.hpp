#define FrameBack {0,0,0,0.5}
#define BUTAC {0.7,0.15,0.1,1}
class TRACKED_DIALOG
{	
	idd = 1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};

};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Secondary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Smoke:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Tracked Vehicle spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "Tanks";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'TANK_DIALOG'";
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Anti Air";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'AA_DIALOG'";
};
class APC_But: RscButton
{
	idc = 1604;
	text = "APCs&MLRS";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'APC_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position TRACKEDBORDER);_Plane setDir (direction TRACKEDBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};

};
};
};

class TANK_DIALOG
{	
	idd = -1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};

class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Tracked Vehicles spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "Tanks";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Anti Air";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'AA_DIALOG'";
};
class APC_But: RscButton
{
	idc = 1604;
	text = "APCs&MLRS";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'APC_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position TRACKEDBORDER);_Plane setDir (direction TRACKEDBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "T34";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','T34_TK_GUE_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   ZIS-S-35'];ctrlSetText[8001,'Secondary:   DT'];ctrlSetText[8002,'Another:  DT Hydras'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:     -']";

};
class A10_but: RscButton
{
	idc = 1607;
	text = "T55"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','T55_TK_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   D-30'];ctrlSetText[8001,'Secondary:   SGMT'];ctrlSetText[8002,'Another:   -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:   -']";
};
class AV8B_But: RscButton
{
	idc = 1608;
	text = "T72";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','T72_RU'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   D-81'];ctrlSetText[8001,'Secondary:   PKT'];ctrlSetText[8002,'Another:   DShkm'];ctrlSetText[8004,'Smoke:   Yes'];ctrlSetText[8003,'Another:   -']";
};
class L39_But: RscButton
{
	idc = 1609;
	text = "T90";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','T90'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   2A46M'];ctrlSetText[8001,'Secondary:   PKT'];ctrlSetText[8002,'Missiles:   9M119M'];ctrlSetText[8004,'Smoke:   Yes'];ctrlSetText[8003,'Another:   Kord']";
};
class KA52_But: RscButton
{
	idc = 1610;
	text = "M1A2 Abrams";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.440581 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','M1A2_US_TUSK_MG_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   M256'];ctrlSetText[8001,'Secondary:   M240'];ctrlSetText[8002,'Another:   M2'];ctrlSetText[8004,'Smoke:   Yes'];ctrlSetText[8003,'Another:   -']";	
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};
};};};
class AA_DIALOG
{	
	idd = -1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};

class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Tracked Vehicles spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "Tanks";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
	action="closeDialog 0;createDialog 'TANK_DIALOG'";
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Anti Air";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	
};
class APC_But: RscButton
{
	idc = 1604;
	text = "APCs&MLRS";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'APC_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position TRACKEDBORDER);_Plane setDir (direction TRACKEDBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "ZSU";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','ZSU_CDF'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   ZIS-S-35'];ctrlSetText[8001,'Secondary:   DT'];ctrlSetText[8002,'Another:  DT Hydras'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:     -']";

};
class A10_but: RscButton
{
	idc = 1607;
	text = "Tunguska"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','2S6M_Tunguska'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   2A38M'];ctrlSetText[8001,'Missiles:   9M311'];ctrlSetText[8002,'Another:   -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:   -']";
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};

};

};
};
class APC_DIALOG
{	
	idd = -1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};

class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Tracked Vehicles spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "Tanks";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
	action="closeDialog 0;createDialog 'TANK_DIALOG'";
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Anti Air";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'AA_DIALOG'";
	
};
class APC_But: RscButton
{
	idc = 1604;
	text = "APCs&MLRS";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position TRACKEDBORDER);_Plane setDir (direction TRACKEDBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class A10_but: RscButton
{
	idc = 1607;
	text = "AAV"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','AAV'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   MK19'];ctrlSetText[8001,'Secondary:   M2'];ctrlSetText[8002,'Another:   -'];ctrlSetText[8004,'Smoke:   Yes'];ctrlSetText[8003,'Another:   -']";
};
class M2A2_but: RscButton
{
	idc = 1608;
	text = "M2A2"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','M2A2_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   M242'];ctrlSetText[8001,'Secondary:   M240'];ctrlSetText[8002,'Missiles:   TOW'];ctrlSetText[8004,'Smoke:   Yes'];ctrlSetText[8003,'Another:   -']";
};
class M2A3_but: RscButton
{
	idc = 1609;
	text = "M2A3"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','M2A3_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   M242'];ctrlSetText[8001,'Secondary:   M240'];ctrlSetText[8002,'Missiles:   TOW'];ctrlSetText[8004,'Smoke:   Yes'];ctrlSetText[8003,'Another:   -']";
};
class M6_but: RscButton
{
	idc = 1610;
	text = "M6"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','M6_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   M242'];ctrlSetText[8001,'Secondary:   M240'];ctrlSetText[8002,'Missiles:   Stinger'];ctrlSetText[8004,'Smoke:   Yes'];ctrlSetText[8003,'Another:   -']";
};
class MLRS_but: RscButton
{
	idc = 1612;
	text = "MLRS"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.440581 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','MLRS_DES_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   MLRS'];ctrlSetText[8001,'Secondary:     -'];ctrlSetText[8002,'Missiles:     -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:   -']";
};
class BMP_but: RscButton
{
	idc = 1611;
	text = "BMP"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.473576 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'BMP_DIALOG'";
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};

};

};
};

class BMP_DIALOG
{	
	idd = -1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};

class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Tracked Vehicles spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "Tanks";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	
	action="closeDialog 0;createDialog 'TANK_DIALOG'";
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Anti Air";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'AA_DIALOG'";
	
};
class APC_But: RscButton
{
	idc = 1604;
	text = "APCs&MLRS";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;	
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position TRACKEDBORDER);_Plane setDir (direction TRACKEDBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class A10_but: RscButton
{
	idc = 1607;
	text = "AAV"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'APC_DIALOG';player setVariable['VehSpawn','AAV'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   MK19'];ctrlSetText[8001,'Secondary:   M2'];ctrlSetText[8002,'Another:   -'];ctrlSetText[8004,'Smoke:   Yes'];ctrlSetText[8003,'Another:   -']";
};
class M2A2_but: RscButton
{
	idc = 1608;
	text = "M2A2"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'APC_DIALOG';player setVariable['VehSpawn','M2A2_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   M242'];ctrlSetText[8001,'Secondary:   M240'];ctrlSetText[8002,'Missiles:   TOW'];ctrlSetText[8004,'Smoke:   Yes'];ctrlSetText[8003,'Another:   -']";
};
class M2A3_but: RscButton
{
	idc = 1609;
	text = "M2A3"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'APC_DIALOG';player setVariable['VehSpawn','M2A4_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   M242'];ctrlSetText[8001,'Secondary:   M240'];ctrlSetText[8002,'Missiles:   TOW'];ctrlSetText[8004,'Smoke:   Yes'];ctrlSetText[8003,'Another:   -']";
};
class M6_but: RscButton
{
	idc = 1610;
	text = "M6"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'APC_DIALOG';player setVariable['VehSpawn','M6_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   M242'];ctrlSetText[8001,'Secondary:   M240'];ctrlSetText[8002,'Missiles:   Stinger'];ctrlSetText[8004,'Smoke:   Yes'];ctrlSetText[8003,'Another:   -']";
};
class MLRS_but: RscButton
{
	idc = 1612;
	text = "MLRS"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.440581 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'APC_DIALOG';player setVariable['VehSpawn','MLRS_DES_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   MLRS'];ctrlSetText[8001,'Secondary:     -'];ctrlSetText[8002,'Missiles:     -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:   -']";
};
class BMP_but: RscButton
{
	idc = 1611;
	text = "BMP"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.473576 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'BMP_DIALOG'";
	colorBackground[] = BUTAC;
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};

};
class AV8B_2_But: RscButton
{
	idc = 1614;
	text = "BMP2";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','BMP2_CDF'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   2A42'];ctrlSetText[8001,'Secondary:    PKT'];ctrlSetText[8002,'Missiles:   Konkurs'];ctrlSetText[8004,'Smoke:   Yes'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_1_But: RscButton
{
	idc = 1613;
	text = "BMP3";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','BMP3'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    2A72'];ctrlSetText[8001,'Secondary:    2A70'];ctrlSetText[8002,'Missiles:   9M117M1'];ctrlSetText[8004,'Smoke:   Yes'];ctrlSetText[8003,'Gunners:   PKT']";
};};};

