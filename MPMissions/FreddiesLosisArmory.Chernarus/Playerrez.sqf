_save = {
        _unit = _this select 0;
        _primaryweapon = primaryWeapon _unit;
        _secondaryweapon = secondaryWeapon _unit;
        _weapons = [_primaryweapon,_secondaryweapon,"NVGoggles","Binocular_Vector"];
        _magazines = magazines _unit;
        _items = items _unit;
        _backpack = unitBackpack _unit;
        _backpack_cargo = [[],[],[]];
        if (!isNull (_backpack)) then {
                _backpack_type = typeOf _backpack;
                _backpack_weapons = [];
                _backpack_magazines = magazines _backpack;
                _backpack_items = items _backpack;
                _backpack_cargo = [_backpack_type,_backpack_weapons,_backpack_magazines,_backpack_items];
        };
        _unit setVariable ["dt_respawn_items",[_weapons,_magazines,_items,_backpack_cargo]];
        
};
_load = {
        _unit = _this select 0;
        _allitems = _unit getVariable ["dt_respawn_items",[[],[],[],["",[],[],[]]]];
        _weapons = _allitems select 0;
        _magazines = _allitems select 1;
        _items = _allitems select 2;
        _backpack = _allitems select 3;
        _backpack_current = unitBackpack _unit;
        removeAllWeapons _unit;
        removeAllItems _unit;
        if (!isNull _backpack_current) then {
                deleteVehicle _backpack_current;
        };
        {
                if (_x != "") then {
                        _unit addWeapon _x;
                };
        } forEach _weapons;
        {
                if (_x != "") then {
                        _unit addMagazine _x;
                };
        } forEach _magazines;
        {
                if (_x != "") then {
                        _unit addWeapon _x;
                };
        } forEach _items;
        _backpack_type = _backpack select 0;
        if (!isNil "_backpack_type" && (_backpack_type) != "") then {
                _backpack_type = _backpack select 0;
                _backpack_weapons = _backpack select 1;
                _backpack_magazines = _backpack select 2;
                _backpack_items = _backpack select 3;
                _unit addBackpack _backpack_type;
                _backpack_unit = unitBackpack _unit;
                {
                        if (_x != "") then {
                                _backpack_unit addWeapon _x;
                        };
                } forEach _backpack_weapons;
                {
                        if (_x != "") then {
                                _backpack_unit addMagazine _x;
                        };
                } forEach _backpack_magazines;
                {
                        if (_x != "") then {
                                _backpack_unit addWeapon _x;
                        };
                } forEach _backpack_items;
        };
        _unit selectWeapon (_weapons select 0);
        _unit setVehicleammo 1;
        
};
_this addEventHandler ["Killed",_save];
_this addEventHandler ["Respawn",_load];