#define FrameBack {0,0,0,0.5}
#define BUTAC {0.7,0.15,0.1,1}
class Wheeled_DIALOG
{	
	idd = 1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};

};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};	
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Secondary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Smoke:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Wheeled Vehicles spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	
};
class Jets_but: RscButton
{
	idc = 1601;
	text = "APCs";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'APCs_DIALOG'";
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Trucks & Jeeps";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'TT_DIALOG'";
};
class APC_But: RscButton
{
	idc = 1604;
	text = "Civilian Vehicles";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'CV_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position WHEELEDBORDER);_Plane setDir (direction WHEELEDBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};

};
};
};

class APCs_DIALOG
{	
	idd = -1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};

class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Wheeled Vehicles spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "APCs";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Trucks & Jeeps";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'TT_DIALOG'";
};
class APC_But: RscButton
{
	idc = 1604;
	text = "Civilan Vehicles";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'CV_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position WHEELEDBORDER);_Plane setDir (direction WHEELEDBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "LAV";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'LAV_DIALOG'";

};
class A10_but: RscButton
{
	idc = 1607;
	text = "Stryker"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'STRYKER_DIALOG'";
};
class AV8B_But: RscButton
{
	idc = 1608;
	text = "BTR";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'BTR_DIALOG'";
};
class L39_But: RscButton
{
	idc = 1609;
	text = "BRDM";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'BRDM_DIALOG'";
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};
};};};

class LAV_DIALOG
{	
	idd = -1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};

class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Wheeled Vehicles spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "APCs";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Trucks & Jeeps";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'TT_DIALOG'";
};
class APC_But: RscButton
{
	idc = 1604;
	text = "Civilan Vehicles";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'CV_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position WHEELEDBORDER);_Plane setDir (direction WHEELEDBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "LAV";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'LAV_DIALOG'";
	colorBackground[] = BUTAC;

};
class A10_but: RscButton
{
	idc = 1607;
	text = "Stryker"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'STRYKER_DIALOG'";
};
class AV8B_But: RscButton
{
	idc = 1608;
	text = "BTR";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'BTR_DIALOG'";
};
class L39_But: RscButton
{
	idc = 1609;
	text = "BRDM";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'BRDM_DIALOG'";
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};
};

class AV8B_1_But: RscButton
{
	idc = 1613;
	text = "LAV25";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','LAV25'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    M242'];ctrlSetText[8001,'Secondary:    M240'];ctrlSetText[8002,'Another:    -'];ctrlSetText[8004,'Smoke:   Yes'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_2_But: RscButton
{
	idc = 1614;
	text = "LAV25 HQ";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','LAV25_HQ'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Another:    M240'];ctrlSetText[8004,'Smoke:   Yes'];ctrlSetText[8003,'Another:    -']";
};
};};
class STRYKER_DIALOG
{	
	idd = -1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};

class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Wheeled Vehicles spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "APCs";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Trucks & Jeeps";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'TT_DIALOG'";
};
class APC_But: RscButton
{
	idc = 1604;
	text = "Civilan Vehicles";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'CV_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position WHEELEDBORDER);_Plane setDir (direction WHEELEDBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "LAV";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'LAV_DIALOG'";
	

};
class A10_but: RscButton
{
	idc = 1607;
	text = "Stryker"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
};
class AV8B_But: RscButton
{
	idc = 1608;
	text = "BTR";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'BTR_DIALOG'";
};
class L39_But: RscButton
{
	idc = 1609;
	text = "BRDM";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'BRDM_DIALOG'";
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};
};

class AV8B_1_But: RscButton
{
	idc = 1613;
	text = "Stryker M2";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','M1126_ICV_M2_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    M2'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Another:    -'];ctrlSetText[8004,'Smoke:   Yes'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_2_But: RscButton
{
	idc = 1614;
	text = "Stryker Mk19";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','M1126_ICV_mk19_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    MK19'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Another:     -'];ctrlSetText[8004,'Smoke:   Yes'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_3_But: RscButton
{
	idc = 1615;
	text = "Stryker MGS";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','M1128_MGS_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    M68'];ctrlSetText[8001,'Secondary:    M2'];ctrlSetText[8002,'Another:     -'];ctrlSetText[8004,'Smoke:   Yes'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_4_But: RscButton
{
	idc = 1616;
	text = "Stryker TOW";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','M1135_ATGMV_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    TOW'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Another:     -'];ctrlSetText[8004,'Smoke:   Yes'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_5_But: RscButton
{
	idc = 1617;
	text = "Stryker Mortar";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.440581 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','M1129_MC_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    Mortar'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Another:     -'];ctrlSetText[8004,'Smoke:   Yes'];ctrlSetText[8003,'Another:    -']";
};
};};
class BTR_DIALOG
{	
	idd = -1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};

class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Wheeled Vehicles spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "APCs";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Trucks & Jeeps";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'TT_DIALOG'";
};
class APC_But: RscButton
{
	idc = 1604;
	text = "Civilan Vehicles";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'CV_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position WHEELEDBORDER);_Plane setDir (direction WHEELEDBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "LAV";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'LAV_DIALOG'";
	

};
class A10_but: RscButton
{
	idc = 1607;
	text = "Stryker"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'STRYKER_DIALOG'";
};
class AV8B_But: RscButton
{
	idc = 1608;
	text = "BTR";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
	};
class L39_But: RscButton
{
	idc = 1609;
	text = "BRDM";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'BRDM_DIALOG'";
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};
};

class AV8B_1_But: RscButton
{
	idc = 1613;
	text = "BTR40";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','BTR40_MG_TK_INS_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    DsHkm'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Another:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_2_But: RscButton
{
	idc = 1614;
	text = "BTR60";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','BTR60_TK_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    KPVT'];ctrlSetText[8001,'Secondary:    PKT'];ctrlSetText[8002,'Another:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_3_But: RscButton
{
	idc = 1615;
	text = "BTR90";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','BTR90'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    2A42'];ctrlSetText[8001,'Secondary:    PKT'];ctrlSetText[8002,'Missiles:    Konkurs'];ctrlSetText[8004,'Smoke:   Yes'];ctrlSetText[8003,'Another:    -']";
};
};};
class BRDM_DIALOG
{	
	idd = -1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};

class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Wheeled Vehicles spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "APCs";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Trucks & Jeeps";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'TT_DIALOG'";
};
class APC_But: RscButton
{
	idc = 1604;
	text = "Civilan Vehicles";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'CV_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position WHEELEDBORDER);_Plane setDir (direction WHEELEDBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "LAV";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'LAV_DIALOG'";
	

};
class A10_but: RscButton
{
	idc = 1607;
	text = "Stryker"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'STRYKER_DIALOG'";
};
class AV8B_But: RscButton
{
	idc = 1608;
	text = "BTR";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'BTR_DIALOG'";
	};
class L39_But: RscButton
{
	idc = 1609;
	text = "BRDM";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};
};
class AV8B_1_But: RscButton
{
	idc = 1613;
	text = "BRDM 2 AT5";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','BRDM2_ATGM_CDF'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:  AT5 Launcher'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Another:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_2_But: RscButton
{
	idc = 1614;
	text = "BDRM2 KPVT";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','BRDM2_CDF'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    KPVT'];ctrlSetText[8001,'Secondary:    PKT'];ctrlSetText[8002,'Another:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_3_But: RscButton
{
	idc = 1615;
	text = "BRDM2 PKT";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','BRDM2_HQ_Gue'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    PKT'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
};};
class TT_DIALOG
{	
	idd = -1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};

class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Wheeled Vehicles spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "APCs";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'APCs_DIALOG'";
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Trucks & Jeeps";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
	
};
class APC_But: RscButton
{
	idc = 1604;
	text = "Civilan Vehicles";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'CV_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position WHEELEDBORDER);_Plane setDir (direction WHEELEDBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "HMMWV";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'HMMWV_DIALOG'";
	

};
class A10_but: RscButton
{
	idc = 1607;
	text = "PickUp"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'PickUp_DIALOG'";
};
class AV8B_But: RscButton
{
	idc = 1608;
	text = "UAZ";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'UAZ_DIALOG'";
	};
class L39_But: RscButton
{
	idc = 1609;
	text = "Ural";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'Ural_DIALOG'";	
};
class L392_But: RscButton
{
	idc = 1610;
	text = "MTVR";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.440581 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'MTVR_DIALOG'";	
};
class L393_But: RscButton
{
	idc = 1611;
	text = "Vodnik";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.473576 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH
	action="closeDialog 0;createDialog 'Vodnik_DIALOG'";	
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};
};
};};
class HMMWV_DIALOG
{	
	idd = -1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};

class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Wheeled Vehicles spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "APCs";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'APCs_DIALOG'";
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Trucks & Jeeps";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
	
};
class APC_But: RscButton
{
	idc = 1604;
	text = "Civilan Vehicles";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'CV_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position WHEELEDBORDER);_Plane setDir (direction WHEELEDBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "HMMWV";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
	

};
class A10_but: RscButton
{
	idc = 1607;
	text = "PickUp"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'PickUp_DIALOG'";
};
class AV8B_But: RscButton
{
	idc = 1608;
	text = "UAZ";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'UAZ_DIALOG'";
	};
class L39_But: RscButton
{
	idc = 1609;
	text = "Ural";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'Ural_DIALOG'";	
};
class L392_But: RscButton
{
	idc = 1610;
	text = "MTVR";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.440581 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'MTVR_DIALOG'";	
};
class L393_But: RscButton
{
	idc = 1611;
	text = "Vodnik";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.473576 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH
	action="closeDialog 0;createDialog 'Vodnik_DIALOG'";	
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};
};

class AV8B_1_But: RscButton
{
	idc = 1613;
	text = "Armored";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','HMMWV_Armored'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:  M240'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Another:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_2_But: RscButton
{
	idc = 1614;
	text = "Avenger";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','HMMWV_Avenger'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    Stinger'];ctrlSetText[8001,'Secondary:    M3P'];ctrlSetText[8002,'Another:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_3_But: RscButton
{
	idc = 1615;
	text = "M2";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','HMMWV_M2'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    M2'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_4_But: RscButton
{
	idc = 1615;
	text = "TOW";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','HMMWV_TOW'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    TOW'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_5_But: RscButton
{
	idc = 1616;
	text = "MK19";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.440581 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','HMMWV_MK19'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    Mk19'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
};};
class PickUp_DIALOG
{	
	idd = -1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};

class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Wheeled Vehicles spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "APCs";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'APCs_DIALOG'";
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Trucks & Jeeps";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
	
};
class APC_But: RscButton
{
	idc = 1604;
	text = "Civilan Vehicles";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'CV_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position WHEELEDBORDER);_Plane setDir (direction WHEELEDBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "HMMWV";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'HMMWV_DIALOG'";
	

};
class A10_but: RscButton
{
	idc = 1607;
	text = "PickUp"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
};
class AV8B_But: RscButton
{
	idc = 1608;
	text = "UAZ";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'UAZ_DIALOG'";
	};
class L39_But: RscButton
{
	idc = 1609;
	text = "Ural";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'Ural_DIALOG'";	
};
class L392_But: RscButton
{
	idc = 1610;
	text = "MTVR";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.440581 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'MTVR_DIALOG'";	
};
class L393_But: RscButton
{
	idc = 1611;
	text = "Vodnik";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.473576 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH
	action="closeDialog 0;createDialog 'Vodnik_DIALOG'";	
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};
};

class AV8B_1_But: RscButton
{
	idc = 1613;
	text = "DShkm";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','Offroad_DSHKM_INS'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:  DShkm'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Another:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_2_But: RscButton
{
	idc = 1614;
	text = "SPG9";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','Offroad_SPG9_Gue'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    SPG9'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Another:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_3_But: RscButton
{
	idc = 1615;
	text = "PKT";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','Pickup_PK_GUE'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    PKT'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
};};
class UAZ_DIALOG
{	
	idd = -1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};

class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Wheeled Vehicles spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "APCs";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'APCs_DIALOG'";
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Trucks & Jeeps";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
	
};
class APC_But: RscButton
{
	idc = 1604;
	text = "Civilan Vehicles";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'CV_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position WHEELEDBORDER);_Plane setDir (direction WHEELEDBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "HMMWV";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'HMMWV_DIALOG'";
	

};
class A10_but: RscButton
{
	idc = 1607;
	text = "PickUp"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'Pickup_DIALOG'";
	
};
class AV8B_But: RscButton
{
	idc = 1608;
	text = "UAZ";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
	};
class L39_But: RscButton
{
	idc = 1609;
	text = "Ural";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'Ural_DIALOG'";	
};
class L392_But: RscButton
{
	idc = 1610;
	text = "MTVR";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.440581 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'MTVR_DIALOG'";	
};
class L393_But: RscButton
{
	idc = 1611;
	text = "Vodnik";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.473576 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH
	action="closeDialog 0;createDialog 'Vodnik_DIALOG'";	
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};
};

class AV8B_1_But: RscButton
{
	idc = 1613;
	text = "DShkm";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','UAZ_MG_CDF'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:  DShkm'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Another:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_2_But: RscButton
{
	idc = 1614;
	text = "SPG9";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','UAZ_SPG9_INS'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    SPG9'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Another:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_3_But: RscButton
{
	idc = 1615;
	text = "AGS30";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','UAZ_AGS30_INS'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    AGS30'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
};};
class Ural_DIALOG
{	
	idd = -1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};

class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Wheeled Vehicles spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "APCs";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'APCs_DIALOG'";
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Trucks & Jeeps";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
	
};
class APC_But: RscButton
{
	idc = 1604;
	text = "Civilan Vehicles";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'CV_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position WHEELEDBORDER);_Plane setDir (direction WHEELEDBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "HMMWV";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'HMMWV_DIALOG'";
	

};
class A10_but: RscButton
{
	idc = 1607;
	text = "PickUp"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'Pickup_DIALOG'";
	
};
class AV8B_But: RscButton
{
	idc = 1608;
	text = "UAZ";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'UAZ_DIALOG'";
	};
class L39_But: RscButton
{
	idc = 1609;
	text = "Ural";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;	
};
class L392_But: RscButton
{
	idc = 1610;
	text = "MTVR";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.440581 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'MTVR_DIALOG'";	
};
class L393_But: RscButton
{
	idc = 1611;
	text = "Vodnik";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.473576 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH
	action="closeDialog 0;createDialog 'Vodnik_DIALOG'";	
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};
};

class AV8B_1_But: RscButton
{
	idc = 1613;
	text = "ZU23 Anti Air";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','Ural_ZU23_Gue'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:  ZU23'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Another:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_2_But: RscButton
{
	idc = 1614;
	text = "Transporter";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','Ural_INS'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Another:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_3_But: RscButton
{
	idc = 1615;
	text = "Ammotruck";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','UralReammo_INS'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_4_But: RscButton
{
	idc = 1616;
	text = "Refueltruck";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','UralRefuel_INS'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_5_But: RscButton
{
	idc = 1617;
	text = "Repairltruck";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.440581 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','UralRepair_INS'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
};};
class MTVR_DIALOG
{	
	idd = -1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};

class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Wheeled Vehicles spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "APCs";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'APCs_DIALOG'";
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Trucks & Jeeps";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
	
};
class APC_But: RscButton
{
	idc = 1604;
	text = "Civilan Vehicles";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'CV_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position WHEELEDBORDER);_Plane setDir (direction WHEELEDBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "HMMWV";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'HMMWV_DIALOG'";
	

};
class A10_but: RscButton
{
	idc = 1607;
	text = "PickUp"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'Pickup_DIALOG'";
	
};
class AV8B_But: RscButton
{
	idc = 1608;
	text = "UAZ";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'UAZ_DIALOG'";
	};
class L39_But: RscButton
{
	idc = 1609;
	text = "Ural";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'URAL_DIALOG'";	
};
class L392_But: RscButton
{
	idc = 1610;
	text = "MTVR";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.440581 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
		
};
class L393_But: RscButton
{
	idc = 1611;
	text = "Vodnik";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.473576 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH
	action="closeDialog 0;createDialog 'Vodnik_DIALOG'";	
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};
};
class AV8B_2_But: RscButton
{
	idc = 1614;
	text = "Transporter";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','MTVR'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Another:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_3_But: RscButton
{
	idc = 1615;
	text = "Ammotruck";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','MtvrReammo'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_4_But: RscButton
{
	idc = 1616;
	text = "Refueltruck";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','Mtvrrefuel'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_5_But: RscButton
{
	idc = 1617;
	text = "Repairltruck";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','MtvrRepair'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
};};
class Vodnik_DIALOG
{	
	idd = -1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};

class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Wheeled Vehicles spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "APCs";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'APCs_DIALOG'";
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Trucks & Jeeps";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
	
};
class APC_But: RscButton
{
	idc = 1604;
	text = "Civilan Vehicles";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'CV_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position WHEELEDBORDER);_Plane setDir (direction WHEELEDBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "HMMWV";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'HMMWV_DIALOG'";
	

};
class A10_but: RscButton
{
	idc = 1607;
	text = "PickUp"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'Pickup_DIALOG'";
	
};
class AV8B_But: RscButton
{
	idc = 1608;
	text = "UAZ";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'UAZ_DIALOG'";
	};
class L39_But: RscButton
{
	idc = 1609;
	text = "Ural";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'URAL_DIALOG'";	
};
class L392_But: RscButton
{
	idc = 1610;
	text = "MTVR";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.440581 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'MTVR_DIALOG'";
		
};
class L393_But: RscButton
{
	idc = 1611;
	text = "Vodnik";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.473576 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH
	colorBackground[] = BUTAC;
	
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};
};
class AV8B_2_But: RscButton
{
	idc = 1614;
	text = "Vodnik";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','GAZ_Vodnik'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Gunner1:    PKT'];ctrlSetText[8001,'Gunner2:    PKT'];ctrlSetText[8002,'Another:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_3_But: RscButton
{
	idc = 1615;
	text = "HMG";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','GAZ_Vodnik_HMG'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    24A2'];ctrlSetText[8001,'Secondary:    PKT'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
class AV8B_4_But: RscButton
{
	idc = 1616;
	text = "MedEvec";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','GAZ_Vodnik_MedEvac'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
};
};};
class CV_DIALOG
{	
	idd = -1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};

class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Wheeled Vehicles spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "APCs";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'APCs_DIALOG'";
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Trucks & Jeeps";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'TT_DIALOG'";
};
class APC_But: RscButton
{
	idc = 1604;
	text = "Civilan Vehicles";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position WHEELEDBORDER);_Plane setDir (direction WHEELEDBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "Bus";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','Ikarus_TK_CIV_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
	

};
class A10_but: RscButton
{
	idc = 1607;
	text = "Lada"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','Lada2_TK_CIV_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
	
};
class AV8B_But: RscButton
{
	idc = 1608;
	text = "Landrover";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','LandRover_TK_CIV_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
	};
class L39_But: RscButton
{
	idc = 1609;
	text = "Paper car";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','Papercar'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,''];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";	
};
class L392_But: RscButton
{
	idc = 1610;
	text = "Tractor";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.440581 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','tractor'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
		
};
class L3922_But: RscButton
{
	idc = 1610;
	text = "Towingtractor";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.473576 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','TowingTractor'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
		
};
class L3923_But: RscButton
{
	idc = 1610;
	text = "Golf";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.506571 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','VWGolf'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
		
};
class L3924_But: RscButton
{
	idc = 1610;
	text = "SUV";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.539566 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','SUV_TK_CIV_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Secondary:    -'];ctrlSetText[8002,'Missiles:    -'];ctrlSetText[8004,'Smoke:   No'];ctrlSetText[8003,'Another:    -']";
		
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};
};
};};