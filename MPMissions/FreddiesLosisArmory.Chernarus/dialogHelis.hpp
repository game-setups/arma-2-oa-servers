#define FrameBack {0,0,0,0.5}
#define BUTAC {0.7,0.15,0.1,1}
class HELI_DIALOG
{	
	idd = 1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};

};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Heli spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "Attack Helicopter";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'MHELI_DIALOG'";
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Transp. Helicopter";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'THELI_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position HELIBORDER);_Plane setDir (direction HELIBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};

};
};
};

class MHELI_DIALOG
{	
	idd = -1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};

class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Heli spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "Attack Helicopter";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Transp. Helicopter";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'THELI_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position HELIBORDER);_Plane setDir (direction HELIBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "AH1Z Cobra";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','AH1Z'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   M-197'];ctrlSetText[8001,'Missiles:   Hellfire'];ctrlSetText[8002,'Missiles2:   Hydras'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Missiles3:   Sidewinder']";

};
class A10_but: RscButton
{
	idc = 1607;
	text = "AH64 Apache"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','AH64D_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   M-230'];ctrlSetText[8001,'Missiles:   Hellfire'];ctrlSetText[8002,'Missiles2:   Hydras'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Missiles3:   -']";
};
class AV8B_But: RscButton
{
	idc = 1608;
	text = "MI-24 Hind";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','Mi24_D_TK_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   Yak-B'];ctrlSetText[8001,'Missiles:   Shturm 9k114'];ctrlSetText[8002,'Missiles2:   S-8'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Missiles3:   -']";
};
class L39_But: RscButton
{
	idc = 1609;
	text = "AH-6";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closedialog 0;createDialog 'AH6_DIALOG';ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Missiles:    -'];ctrlSetText[8002,'Missiles2:     -'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Missiles3:     -']";
};
class KA52_But: RscButton
{
	idc = 1610;
	text = "KA-52";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.440581 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','KA52'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   Yak-B'];ctrlSetText[8001,'Missiles:   Shturm 9k114'];ctrlSetText[8002,'Missiles2:   S-8'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Missiles3:   -']";	
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};

};

};
};
class AH6_DIALOG
{	
	idd = -1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};

class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Heli spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "Attack Helicopter";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog ''";
	colorBackground[] = BUTAC;
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Transp. Helicopter";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'THELI_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position HELIBORDER);_Plane setDir (direction HELIBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "AH1Z Cobra";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','AH1Z'];closeDialog 0;createDialog 'MHELI_DIALOG';_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   M-197'];ctrlSetText[8001,'Missiles:   Hellfire'];ctrlSetText[8002,'Missiles2:   Hydras'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Missiles3:   Sidewinder']";

};
class A10_but: RscButton
{
	idc = 1607;
	text = "AH64 Apache"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','AH64D_EP1'];closeDialog 0;createDialog 'MHELI_DIALOG';_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   M-230'];ctrlSetText[8001,'Missiles:   Hellfire'];ctrlSetText[8002,'Missiles2:   Hydras'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Missiles3:   -']";
};
class AV8B_But: RscButton
{
	idc = 1608;
	text = "MI-24 Hind";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','Mi24_D_TK_EP1'];closeDialog 0;createDialog 'MHELI_DIALOG';_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   Yak-B'];ctrlSetText[8001,'Missiles:   Shturm 9k114'];ctrlSetText[8002,'Missiles2:   S-8'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Missiles3:   -']";
};
class L39_But: RscButton
{
	idc = 1609;
	text = "AH-6";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
};
class KA52_But: RscButton
{
	idc = 1610;
	text = "KA-52";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.440581 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','KA52'];closeDialog 0;createDialog 'MHELI_DIALOG';_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   Yak-B'];ctrlSetText[8001,'Missiles:   Shturm 9k114'];ctrlSetText[8002,'Missiles2:   S-8'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Missiles3:   -']";
};
class AV8B_1_But: RscButton
{
	idc = 1613;
	text = "Unarmed";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','AH6X_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Missiles:    -'];ctrlSetText[8002,'Missiles2:    -'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Missiles3:    -']";
};
class AV8B_2_But: RscButton
{
	idc = 1614;
	text = "Armed";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','AH6J_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   M134'];ctrlSetText[8001,'Missiles:   Hydras'];ctrlSetText[8002,'Missiles2:    -'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Missiles3:    -']";
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};
};};};
class THELI_DIALOG
{	
	idd = 3;
	movingenable = true;

class controls
{
class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Heli spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};
class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "Attack Helicopter";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'MHELI_DIALOG'";
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Transp. Helicopter";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
	action="";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position HELIBORDER);_Plane setDir (direction HELIBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closedialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "Chinook";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = {0.7,0.15,0.1,1};
	action="player setVariable['VehSpawn','CH_47F_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Gunner1:   M134'];ctrlSetText[8001,'Gunner2:   M134'];ctrlSetText[8002,'Gunner3:   M240'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Missiles3:   -']";
};
class MI_but: RscButton
{
	idc = 1607;
	text = "MI-17";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDIalog 'MI_DIALOG'";
};
class UH_but: RscButton
{
	idc = 1608;
	text = "UH-";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDIalog 'UH_DIALOG'";
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};
};};};
class MI_DIALOG
{	
	idd = 3;
	movingenable = true;

class controls
{
class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Heli spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};
class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "Attack Helicopter";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'MHELI_DIALOG'";
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Transp. Helicopter";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
	action="";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position HELIBORDER);_Plane setDir (direction HELIBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closedialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "Chinook";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','CH_47F_EP1'];closedialog 0;createDialog 'THELI_DIALOG';_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Gunner1:   M134'];ctrlSetText[8001,'Gunner2:   M134'];ctrlSetText[8002,'Gunner3:   M240'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Missiles3:   -']";
};
class MI_but: RscButton
{
	idc = 1607;
	text = "MI-17";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	ColorBackground[] = BUTAC;
};
class UH_but: RscButton
{
	idc = 1608;
	text = "UH-";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDIalog 'UH_DIALOG'";
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};
};
class AV8B_1_But: RscButton
{
	idc = 1613;
	text = "Mi-17Medevac";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','Mi17_medevac_RU'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Missiles:    -'];ctrlSetText[8002,'Missiles2:    -'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Missiles3:    -']";
};
class AV8B_2_But: RscButton
{
	idc = 1614;
	text = "Mi-17 UN";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','Mi17_UN_CDF_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Gunner1:   PKT'];ctrlSetText[8002,'Gunner2:    PKT'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Missiles:    -']";
};
class AV8B_3_But: RscButton
{
	idc = 1615;
	text = "Mi-17 Rockets";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','Mi17_rockets_RU'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    S-5'];ctrlSetText[8001,'Gunner1:   PKT'];ctrlSetText[8002,'Gunner2:    PKT'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Gunner3:   PKT']";
};
class AV8B_4_But: RscButton
{
	idc = 1616;
	text = "Mi-17 Normal";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','Mi17_TK_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Gunner1:   PKT'];ctrlSetText[8002,'Gunner2:    PKT'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Gunner3:   PKT']";
};
};};
class UH_DIALOG
{	
	idd = 3;
	movingenable = true;

class controls
{
class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Heli spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};
class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "Attack Helicopter";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'MHELI_DIALOG'";
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Transp. Helicopter";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = BUTAC;
	action="";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable 'VehSpawn';_Plane = _vehicle createVehicle (position HELIBORDER);_Plane setDir (direction HELIBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closedialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "Chinook";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','CH_47F_EP1'];closedialog 0;createDialog 'THELI_DIALOG';_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Gunner1:   M134'];ctrlSetText[8001,'Gunner2:   M134'];ctrlSetText[8002,'Gunner3:   M240'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Missiles3:   -']";
};
class MI_but: RscButton
{
	idc = 1607;
	text = "MI-17";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
		action="closeDialog 0;createDIalog 'MI_DIALOG'";

};
class UH_but: RscButton
{
	idc = 1608;
	text = "UH-";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	ColorBackground[] = BUTAC;
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};
};
class AV8B_1_But: RscButton
{
	idc = 1613;
	text = "UH-1H";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','UH1H_TK_GUE_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Gunner1:   M240'];ctrlSetText[8002,'Gunner2:   M240'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Missiles:    -']";
};
class AV8B_2_But: RscButton
{
	idc = 1614;
	text = "UH-60M";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','UH60M_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Gunner1:   M134'];ctrlSetText[8002,'Gunner2:    M134'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Missiles:    -']";
};
class AV8B_3_But: RscButton
{
	idc = 1615;
	text = "UH-60M Medevec";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','UH60M_MEV_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Gunner1:   -'];ctrlSetText[8002,'Gunner2:    -'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Gunner3:   -']";
};
class AV8B_4_But: RscButton
{
	idc = 1616;
	text = "UH-1Y";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','UH1Y'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Gunner1:   M134'];ctrlSetText[8002,'Gunner2:    M134'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Gunner3:   -']";
};
};};