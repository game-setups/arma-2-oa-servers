systemChat("Freddie's Armory: Correction filter enabled! Hit F3 to toggle!");
FreddiesFilterEnabled = true;
SaturationAmount = 0.92; // 1 = Default, decrease to increase saturation

FredFilter = ppEffectCreate ["colorCorrections", 1501];
FredFilter ppEffectEnable true;
FredFilter ppEffectAdjust[ 1, 1, 0, [0.1, -0.09, 0, 0],[5, -0.09, -5, SaturationAmount],[1.66, -1.03, -1.05, 0.07]];
FredFilter ppEffectCommit 0;


if (!isnil "FilterHotkey") then
{
	(findDisplay 46) displayRemoveEventHandler ["KeyDown", FilterHotkey];
};

FilterKeybind =
{
	switch (_this) do
	{
		case 61: //F3 key
		{
			if FreddiesFilterEnabled then {
				FredFilter ppEffectEnable false;
				systemChat("Freddie's Armory: Filter disabled");
				
				FreddiesFilterEnabled = false;
			} else {
				FredFilter ppEffectEnable true;
				systemChat("Freddie's Armory: Filter enabled");
				
				FreddiesFilterEnabled = true;
			};
		};
	};
};

waituntil {!isnull (finddisplay 46)};
FilterHotkey = (findDisplay 46) displayAddEventHandler ["KeyDown", "_this select 1 call FilterKeybind; false;"];




/* Original code that only allows the filter to be enabled and not disabled
systemChat("Freddie's filter applied!");

"colorCorrections" ppEffectEnable false;
"chromAberration" ppEffectEnable false;
"radialBlur" ppEffectEnable false;
"filmGrain" ppEffectEnable false;

_hndl = ppEffectCreate ["colorCorrections", 1501];
_hndl ppEffectEnable true;
_hndl ppEffectAdjust[ 1, 1, 0, [0.1, -0.09, 0, 0],[5, -0.09, -5, 0.88],[1.66, -1.03, -1.05, 0.07]];
_hndl ppEffectCommit 0;

player removeAction actionFreddiesFilter;

/*
Stock arma 2 (clear day):
https://gyazo.com/fd16ae862b81270e0d0dd6e26c47cabf

My custom filter:
https://gyazo.com/1051401f741bf6e2013a3fc8a557d36b