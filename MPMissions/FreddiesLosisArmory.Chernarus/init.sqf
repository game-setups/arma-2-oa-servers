setViewDistance 3000;
setDate [2014, 0, 0, 12, 0];
0 setFog 0;
bis_fog = 0;
0 setovercast 0;
0 setRain 0;

systemChat("Freddie's Armory: Gear saving enabled!");

//cutText [format["Welcome %1, to Freddie's Armory Server!", name player], "PLAIN"];
////"Developed by Freddie"
//hintC parsetext "
//<t size='1.2' color='#FF0000'>Armory Rules:<t/><br/>
//<t size='1.2' color='#FF0000'>1. Be mindful of your actions and who you may annoy<t/><br/>
//<t size='1.2' color='#FF0000'>2. Use of hacked in material will result in a permanent ban! DON'T EVEN PICK IT UP!<t/><br/>
//<t size='1.2' color='#FF0000'>3. Don't conseal hackers<t/><br/>
//<t size='1.2' color='#FF0000'>4. Play to have fun, don't get stressed, just have a laugh!<t/><br/>
//";


//Loads AH or admin hotkey for admins
[] spawn
{
	systemChat("Freddie's Armory: Loading Freddie's anti-script and admin system...");
	_adminpidarray = ["76561198344844414"];		// Order: Freddie
	if (getPlayerUID(player) in _adminpidarray) then
	{
		systemChat("Freddie's Armory: Admin system loaded! You've logged in as an admin! Press F2 to run freddie_admin.sqf");
		if (!isnil "FreddieHotKeys") then
		{
			(findDisplay 46) displayRemoveEventHandler ["KeyDown", FreddieHotKeys];
		};

		Freddieskeybind =
		{
			switch (_this) do 
			{
				case 60: //F2 key
				{
					systemChat("Freddie's Armory: Executed ""freddie_admin.sqf""");
					execVM format['freddie_admin.sqf'];
				};
			};
		};

		waituntil {!isnull (finddisplay 46)};
		FreddieHotKeys = (findDisplay 46) displayAddEventHandler ["KeyDown", "_this select 1 call Freddieskeybind; false;"];
	} else {
		execVM format['antiscript.sqf'];
		systemChat("Freddie's Armory: Freddie's anti-script and admin system loaded");
	};
};


//Adds option to enable my filter
[] spawn
{
	systemChat("Freddie's Armory: Loaded filter");
	//actionFreddiesFilter = (vehicle player) addAction [("<t color=""#00FFFF"">" + ("Add filter") + "</t>"), "freddiesfilter.sqf"];
	execVM("freddiesfilter.sqf");
};


//Removes wrecks every 5 mins
[] spawn
{
	systemChat("Freddie's Armory: Loaded wreck removal");
	while {true} do
	{
		_nu = (position Spawnprotector) nearObjects 2500; { if (getDammage _x >= 1 && (typeOf _x != "ProtectionZone_Ep1")) then {deleteVehicle _x;}; } forEach _nu;
		_cu = (position Spawnprotector) nearObjects 2500; { if (typeOf _x == "CraterLong") then {deleteVehicle _x;}; } forEach _cu;
		0 setFog 0;
		bis_fog = 0;
		0 setovercast 0;
		0 setRain 0;
		sleep 300;
	};
};