#define FrameBack {0,0,0,0.5}
class JET_DIALOG
{	
	idd = 1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};

};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Jet spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "Military Jets";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'MJETS_DIALOG'";
};
class Trans_But: RscButton
{
	idc = 1602;
	text = "Transport Planes";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'TJETS_DIALOG'";
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Civil Planes";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'CJETS_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position JETBORDER);_Plane setDir (direction JETBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};

};
};
};

class MJETS_DIALOG
{	
	idd = -1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};

class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Jet spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "Military Jets";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = {0.7,0.15,0.1,1};
};
class Trans_But: RscButton
{
	idc = 1602;
	text = "Transport Planes";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog'TJETS_DIALOG'";
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Civil Planes";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog'CJETS_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable 'VehSpawn';_Plane = _vehicle createVehicle (position JETBORDER);_Plane setDir (direction JETBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class F35B_but: RscButton
{	idc = 1606;
	text = "F35B";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','F35B'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   Gau-12'];ctrlSetText[8001,'Missiles:   Sidewinder'];ctrlSetText[8002,'Missiles2:     -'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Bombs:   GBU-2']";

};
class A10_but: RscButton
{
	idc = 1607;
	text = "A10"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','A10'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   Gau-8'];ctrlSetText[8001,'Missiles:   Sidewinder'];ctrlSetText[8002,'Missiles2:Maverick&Hydras'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Bombs:   GBU-2']";
};
class AV8B_But: RscButton
{
	idc = 1608;
	text = "AV8B";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','AV8B2'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   Gau-12'];ctrlSetText[8001,'Missiles:   Sidewinder'];ctrlSetText[8002,'Missiles2:   Hydras'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Bombs:   MK-82']";
};
class L39_But: RscButton
{
	idc = 1609;
	text = "L39";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.407586 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','L39_TK_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   Gsh-23L'];ctrlSetText[8001,'Missiles:   S-5'];ctrlSetText[8002,'Missiles2:     -'];ctrlSetText[8004,'Flares:   No'];ctrlSetText[8003,'Bombs:     -']";
};
class SU25_But: RscButton
{
	idc = 1610;
	text = "SU-25";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.440581 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','SU25_TK_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   Gsh-301'];ctrlSetText[8001,'Missiles:   S-8'];ctrlSetText[8002,'Missiles2:   R-73'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Bombs:   Fab-250']";
};
class SU34_But: RscButton
{
	idc = 1611;
	text = "SU-34";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.473576 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','SU34'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:   Gsh-301'];ctrlSetText[8001,'Missiles:   S-8'];ctrlSetText[8002,'Missiles2:   R-73'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Bombs:   CH-29']";
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};

};

};
};

class TJETS_DIALOG
{	
	idd = -1;
	movingenable = true;

class controls
{

class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};

class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Jet spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};

class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "Military Jets";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'MJETS_DIALOG'";
};
class Trans_But: RscButton
{
	idc = 1602;
	text = "Transport Planes";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = {0.7,0.15,0.1,1};
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Civil Planes";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog'CJETS_DIALOG'";
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable ""VehSpawn"";_Plane = _vehicle createVehicle (position JETBORDER);_Plane setDir (direction JETBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closeDialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "C130J";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','C130J'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Missiles:    -'];ctrlSetText[8002,'Missiles2:    -'];ctrlSetText[8004,'Flares:   Yes'];ctrlSetText[8003,'Bombs:    -']";
};
class A10_but: RscButton
{
	idc = 1607;
	text = "MV22-Osprey"
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','MV22'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Missiles:    -'];ctrlSetText[8002,'Missiles2:    -'];ctrlSetText[8004,'Flares:   No'];ctrlSetText[8003,'Bombs:    -']";
};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};
};};};
class CJETS_DIALOG
{	
	idd = 3;
	movingenable = true;

class controls
{
class RscText_1000: RscText
{
	idc = 1000;
	x = 0.270833 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.458333 * safezoneW;
	h = 0.549786 * safezoneH;
	colorBackground[] = {0.2,0.2,0.2,0.8};
};

class Vehicles_Back: RscText
{
	idc = 4002;
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.387229 * safezoneH;
	colorBackground[] = FrameBack;
};
class Categories_Back: RscText
{
	idc = 4003;
	x = 0.280357 * safezoneW + safezoneX;
	y = 0.303886 * safezoneH + safezoneY;
	w = 0.0898805 * safezoneW;
	h = 0.104482 * safezoneH;
	colorBackground[] = FrameBack;
};
class Sort_Back: RscText
{
	idc = 4004;
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.304838 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.168267 * safezoneH;
	colorBackground[] = FrameBack;
};
class WEP_Back: RscText
{
	idc = 4005;
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.475248 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.228243 * safezoneH;
	colorBackground[] = FrameBack;
};
class PrimWep: RscText
{
	idc = 8000;
	text = "Primary:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.5 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Secondary_Wep: RscText
{
	idc = 8001;
	text = "Missiles:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.527496 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Bombs_Wep: RscText
{
	idc = 8002;
	text = "Bombs:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.554991 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Another_Wep: RscText
{
	idc = 8003;
	text = "Another:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.582487 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class Flares_Wep: RscText
{
	idc = 8004;
	text = "Flares:";
	x = 0.577362 * safezoneW + safezoneX;
	y = 0.609982 * safezoneH + safezoneY;
	w = 0.13092 * safezoneW;
	h = 0.0530868 * safezoneH;
	colorText[] = {1,1,1,1};
};
class JET_FRAME: RscFrame
{
	idc = 1800;
	text = "Jet spawn menu";
	x = 0.274404 * safezoneW + safezoneX;
	y = 0.225107 * safezoneH + safezoneY;
	w = 0.451785 * safezoneW;
	h = 0.545978 * safezoneH;
};
class Jet_Categories: RscFrame
{
	idc = 1801;
	text = "Categories";
	x = 0.280358 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.111146 * safezoneH;
	colorBackground[] = {0,0,0,1};
	colorBackgroundActive[] = {0,0,0,1};
};
class Jet_Vehicle: RscFrame
{
	idc = 1802;
	text = "Vehicle";
	x = 0.374405 * safezoneW + safezoneX;
	y = 0.298174 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.392941 * safezoneH;
};
class Jet_SORT: RscFrame
{
	idc = 1803;
	text = "Color or Weapons";
	x = 0.469047 * safezoneW + safezoneX;
	y = 0.299126 * safezoneH + safezoneY;
	w = 0.0904758 * safezoneW;
	h = 0.173979 * safezoneH;
};
class RscFrame_1804: RscFrame
{
	idc = 1804;
	x = 0.570833 * safezoneW + safezoneX;
	y = 0.270566 * safezoneH + safezoneY;
	w = 0.146429 * safezoneW;
	h = 0.442447 * safezoneH;
	

};
class Jets_but: RscButton
{
	idc = 1601;
	text = "Military Jets";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog 'MJETS_DIALOG'";
};
class Trans_But: RscButton
{
	idc = 1602;
	text = "Transport Planes";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="closeDialog 0;createDialog'TJETS_DIALOG'";
};
class Civil_But: RscButton
{
	idc = 1603;
	text = "Civil Planes";
	x = 0.282197 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = {0.7,0.15,0.1,1};
};
class JET_PIC: RscPicture
{
	idc = 1200;
	text = "";
	x = 0.575595 * safezoneW + safezoneX;
	y = 0.275326 * safezoneH + safezoneY;
	w = 0.138095 * safezoneW;
	h = 0.19302 * safezoneH;
};
class RscFrame_1805: RscFrame
{
	idc = 1805;
	x = 0.575596 * safezoneW + safezoneX;
	y = 0.474296 * safezoneH + safezoneY;
	w = 0.136905 * safezoneW;
	h = 0.2311 * safezoneH;
};
class Jet_Weps: RscStructuredText
{
	idc = 1101;
	text = "Weapons";
	x = 0.579166 * safezoneW + safezoneX;
	y = 0.478104 * safezoneH + safezoneY;
	w = 0.126785 * safezoneW;
	h = 0.220628 * safezoneH;
	colorBackground[] = {-1,-1,-1,0};
	colorText[] = {1,1,1,1};
};
class SpawnBut: RscButton
{
	idc = 1604;
	text = "Spawn Vehicle";
	x = 0.571615 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action="closeDialog 0;_vehicle =player getVariable 'VehSpawn';_Plane = _vehicle createVehicle (position JETBORDER);_Plane setDir (direction JETBORDER);player moveInDriver _Plane;_Plane engineOn true";
};
class CloseBut: RscButton
{
	idc = 1605;
	text = "Close";
	x = 0.277995 * safezoneW + safezoneX;
	y = 0.719914 * safezoneH + safezoneY;
	w = 0.144643 * safezoneW;
	h = 0.0426025 * safezoneH;
	action = "closedialog 0";
};
class F35B_but: RscButton
{
	idc = 1606;
	text = "AN2";
	x = 0.3765 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	colorBackground[] = {0.7,0.15,0.1,1};

};
class Title: RscStructuredText
{
	idc = 1102;
	text = "Vehicle Spawn System V1       by Losi";
	x = 0.278049 * safezoneW + safezoneX;
	y = 0.238792 * safezoneH + safezoneY;
	w = 0.235031 * safezoneW;
	h = 0.0264969 * safezoneH;
	colorText[] = {1,1,1,1};


};
class AV8B_1_But: RscButton
{
	idc = 1613;
	text = "Green";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.308602 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','An2_TK_CIV_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> 'AN2_1_TK_CIV_EP1' >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Missiles:    -'];ctrlSetText[8002,'Missiles2:    -'];ctrlSetText[8004,'Flares:   No'];ctrlSetText[8003,'Bombs:    -']";
};
class AV8B_2_But: RscButton
{
	idc = 1614;
	text = "White/Red";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.341597 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','AN2_1_TK_CIV_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Missiles:    -'];ctrlSetText[8002,'Missiles2:    -'];ctrlSetText[8004,'Flares:   No'];ctrlSetText[8003,'Bombs:    -']";
};
class AV8B_3_But: RscButton
{
	idc = 1614;
	text = "White/Green";
	x = 0.470841 * safezoneW + safezoneX;
	y = 0.374591 * safezoneH + safezoneY;
	w = 0.086288 * safezoneW;
	h = 0.0235677 * safezoneH;
	action="player setVariable['VehSpawn','An2_2_TK_CIV_EP1'];_vehicle =player getVariable 'VehSpawn';ctrlsetText[1200,getText (configFile >> 'cfgVehicles' >> _vehicle >> 'picture')];ctrlSetText[8000,'Primary:    -'];ctrlSetText[8001,'Missiles:    -'];ctrlSetText[8002,'Missiles2:    -'];ctrlSetText[8004,'Flares:   No'];ctrlSetText[8003,'Bombs:    -']";
};};};